//
//  Constants.swift
//  CoffeeBeanMachine
//
//  Created by Rokas on 31/05/2019.
//  Copyright © 2019 Webfu. All rights reserved.
//

import Foundation

struct Constants {
    static let engineerCount = 10
    static let initialTime = 1
    static let brewDuration = 3
    static var superBusyDuration = 20
    static var superBusyChance = 0.4
    static let coffeeDrinkingDuration = 0
    
}
