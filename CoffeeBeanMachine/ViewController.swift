//
//  ViewController.swift
//  CoffeeBeanMachine
//
//  Created by Rokas on 30/05/2019.
//  Copyright © 2019 Webfu. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ViewController: UIViewController {

    var engineers: [Engineer] = [Engineer]()
    
    func setupEngineers() {
        for i in 0...Constants.engineerCount {
            let engineer = Engineer(id: i)
            engineers.append(engineer)
        }
    }
    
    func startEventStream() {
        for _ in 0...100 {
            EventStream.shared.processEvent()?.execute()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupEngineers()
        startEventStream()
    }
    
}

