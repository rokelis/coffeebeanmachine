//
//  Engineer.swift
//  CoffeeBeanMachine
//
//  Created by Rokas on 31/05/2019.
//  Copyright © 2019 Webfu. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

enum EventType {
    case isSuperBusy(Bool)
    case isWorking(Bool)
}

class Engineer {
    
    let id: Int
    
    var isSuperBusy = PublishSubject<Bool>()
    var isSuperBusyState = false
    var isWorking = PublishSubject<Bool>()
    var timeBeforeCoffee = Int.random(in: 0 ... 30) + 50
    var chanceToSuperBusy:Int = {
            return (10 - Int.random(in: 0 ... 10)) * 10
    }()
    
    init(id: Int) {
        self.id = id
        configureObserver()
        
        self.isSuperBusy.onNext(false)
        self.isWorking.onNext(true)
    }
    
    func configureObserver() {
        let isSuperBusyObservable = isSuperBusy.asObservable().map { EventType.isSuperBusy($0) }
        let isWorkingObservable = isWorking.asObservable().map { EventType.isWorking($0) }
        
        _ = Observable.of(isSuperBusyObservable, isWorkingObservable)
            .merge()
            .subscribe(onNext: { result in
                switch result {
                case .isSuperBusy(let value):
                    if value {
                        let event = self.becomesUsuallyBusy()
                        EventStream.shared.schedule(event: event)
                    } else {
                        let event = self.becomesSuperBusy()
                        EventStream.shared.schedule(event: event)
                    }
                case .isWorking(let value):
                    if value {
                        let event = self.joinsCoffeeQueue()
                        EventStream.shared.schedule(event: event)
                    }
                }
            })
    }

    func becomesSuperBusy() -> Event {
        let delay = chanceToSuperBusy
        return Event(delay: delay,
                     execute: {[weak self] in
                        guard let `self` = self else { return }
                        print ("Engineer \(self.id) becomes super busy")
                        self.isSuperBusy.onNext(true)
                        self.isSuperBusyState = true
                    })
    }
    
    func becomesUsuallyBusy() -> Event {
        return Event(delay: Constants.superBusyDuration,
                     execute: { [weak self] in
                        guard let `self` = self else { return }
                        print ("Engineer \(self.id) becomes usually busy")
                        self.isSuperBusy.onNext(false)
                        self.isSuperBusyState = false
        })
    }
    
    func joinsCoffeeQueue() -> Event {
        return Event(delay: timeBeforeCoffee,
                     execute: { [weak self] in
                        guard let `self` = self else { return }
                        print ("Engineer \(self.id) joins coffee queue")
                        CoffeeMachine.shared.joinsQueue(engineer: self)
                        self.isWorking.onNext(false)
        })
    }
    
    func works() {
        isWorking.onNext(true)
        print ("Coffee is served to \(self), who returns to work")
    }
}

extension Engineer: CustomStringConvertible {
    var description: String {
        return "Engineer \(id)"
    }
    
    
}
