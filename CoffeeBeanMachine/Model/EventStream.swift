//
//  EventStream.swift
//  CoffeeBeanMachine
//
//  Created by Rokas on 01/06/2019.
//  Copyright © 2019 Webfu. All rights reserved.
//

import Foundation

class EventStream {
    static let shared = EventStream()
    private var pq: PriorityQueue<Event> = PriorityQueue<Event>(ascending: true, startingValues: [])
    private var currentTime: Int = 1
    
    private init () {}
    
    func schedule(event: Event) {
        event.time = currentTime + event.delay
        pq.push(event)
    }
    
    func processEvent() -> Event?{
        if let event = pq.pop() {
            currentTime = event.time
            print("Time:\(currentTime)")
            return event
        }
        return nil
    }
}
