//
//  CoffeeMachine.swift
//  CoffeeBeanMachine
//
//  Created by Rokas on 01/06/2019.
//  Copyright © 2019 Webfu. All rights reserved.
//

import Foundation
import RxSwift

class CoffeeMachine {
    static let shared = CoffeeMachine()
    var queue = [Engineer]()
    
    var isBrewing = PublishSubject<Bool>()
    var isBrewingState = false
    
    private init() {}
    
    func startBrewing() -> Event {
        return Event(delay: Constants.brewDuration,
                     execute: {[weak self] in
                        guard let `self` = self else { return }
                        if let engineer = CoffeeMachine.shared.serveFirstInQueue() {
                            engineer.works()
                            
                            print("Queue: \(self.queue)")
                            
                            if self.queue.count > 0 {
                                self.brew()
                            } else {
                                self.isBrewingState = false
                                self.isBrewing.onNext(false)
                            }
                        }
        })
    }
    
    func brew() {
        isBrewingState = true
        isBrewing.onNext(true)
        print("Brewing started")
        EventStream.shared.schedule(event: startBrewing())
    }
    
    func joinsQueue(engineer: Engineer) {
        queue.append(engineer)
        print("Queue: \(queue)")
        if !isBrewingState {
            brew()
        }
    }
    
    func serveFirstInQueue() -> Engineer? {
        if let index = queue.firstIndex(where: { $0.isSuperBusyState }) {
            let engineer = queue[index]
            queue.remove(at: index)
            return engineer
        } else {
            if let engineer = queue.first {
                queue.removeFirst()
                return engineer
            }
        }
        return nil
    }
}
