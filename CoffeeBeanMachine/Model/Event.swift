//
//  Event.swift
//  CoffeeBeanMachine
//
//  Created by Rokas on 31/05/2019.
//  Copyright © 2019 Webfu. All rights reserved.
//

import Foundation

class Event {
    var time: Int = 0
    var delay: Int = 0
    var execute: (() -> Void) = {}
    
    init(delay: Int, execute: @escaping (() -> Void) ) {
        self.delay = delay
        self.execute = execute
    }
}
extension Event: Comparable {
    static func < (lhs: Event, rhs: Event) -> Bool {
        return lhs.time < rhs.time
    }
    
    static func == (lhs: Event, rhs: Event) -> Bool {
        return lhs.time == rhs.time
    }
    
    
}
